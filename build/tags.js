riot.tag2('app', '<h1>Hello!</h1> <p>This is an example</p> <h2>My Form:</h2> <form onsubmit="{handleForm}"> <input placeholder="Name" type="text" ref="name"> <input placeholder="Favorite Color" type="text" ref="favColor"> <input type="submit" value="Ok, lets go!"> </form> <button onclick="{handleDelete}">Delete</button> <div class="wrapper"> <people-list myheader="{header}" myitems="{items}" class="left"></people-list> <people-table myheader="{header}" myitems="{items}" class="right"></people-table> </div>', 'app .wrapper,[data-is="app"] .wrapper{ display: flex; } app .left,[data-is="app"] .left{ flex: 0 0 65%; } app .right,[data-is="app"] .right{ flex: 1; }', '', function(opts) {

        this.header = ['Name', 'Favorite Color']

        this.items = [
            { name: 'Bob', favColor: 'Yellow' },
            { name: 'Michelle', favColor: 'Purple' },
        ]

        this.handleForm = (e) => {
            e.preventDefault()
            const name = this.refs.name.value
            const favColor = this.refs.favColor.value

            this.items = [...this.items, { name, favColor }]

        }

        this.handleDelete = (e) => {
            e.preventDefault()
            this.items.pop()
        }

});
riot.tag2('people-list', '<h2>People List</h2> <ul> <li each="{items}">Name {name} -> Favorite Color: {favColor}</li> </ul>', '', '', function(opts) {
        this.items = opts.myitems
        this.header = opts.myheader

        this.on('update', function () {
            this.items = opts.myitems
            this.header = opts.myheader
        })

});
riot.tag2('people-table', '<h2>People Table</h2> <table> <tr> <th each="{title, i in header}">{title}</th> </tr> <tr each="{item, i in items}"> <td each="{value, key in item}">{value}</td> </tr> </table>', 'people-table table,[data-is="people-table"] table,people-table th,[data-is="people-table"] th,people-table td,[data-is="people-table"] td{ border: 1px solid black; }', '', function(opts) {

        this.items = opts.myitems
        this.header = opts.myheader

        this.on('update', function () {
            this.items = opts.myitems
            this.header = opts.myheader
        })

});