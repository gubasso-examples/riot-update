const path = require('path')

module.exports = {
    from: "src/tags",
    to: path.resolve(__dirname, "build") + "/tags.js"
}