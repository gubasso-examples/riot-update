<app>

    <style>
        .wrapper {
            display: flex;
        }

        .left {
            flex: 0 0 65%;
        }

        .right {
            flex: 1;
        }
    </style>

    <h1>Hello!</h1>

    <p>This is an example</p>

    <h2>My Form:</h2>
    <form onsubmit={handleForm}>
        <input placeholder="Name" type="text" ref="name">
        <input placeholder="Favorite Color" type="text" ref="favColor">
        <!-- <input type="submit" value="Ok, lets go!"> -->
        <input type="submit" value="Ok, lets go!">
    </form>

    <button onclick={handleDelete}>Delete</button>

    <div class="wrapper">
        <people-list myheader={header} myitems={items} class="left" />

        <people-table myheader={header} myitems={items} class="right" />

    </div>

    <script>

        this.header = ['Name', 'Favorite Color']

        this.items = [
            { name: 'Bob', favColor: 'Yellow' },
            { name: 'Michelle', favColor: 'Purple' },
        ]

        this.handleForm = (e) => {
            e.preventDefault()
            const name = this.refs.name.value
            const favColor = this.refs.favColor.value

            this.items = [...this.items, { name, favColor }]

        }

        this.handleDelete = (e) => {
            e.preventDefault()
            this.items.pop()
        }

    </script>

</app>