<people-table>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
        }
    </style>


    <h2>People Table</h2>

    <table>
        <tr>
            <th each='{title, i in header}'>{title}</th>
        </tr>
        <tr each='{item, i in items}'>
            <td each='{value, key in item}'>{value}</td>
        </tr>
    </table>

    <script>

        this.items = opts.myitems
        this.header = opts.myheader

        this.on('update', function () {
            this.items = opts.myitems
            this.header = opts.myheader
        })


    </script>



</people-table>