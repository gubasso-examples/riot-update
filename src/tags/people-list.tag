<people-list>

    <h2>People List</h2>

    <ul>
        <li each={items}>Name {name} -> Favorite Color: {favColor}</li>
    </ul>

    <script>
        this.items = opts.myitems
        this.header = opts.myheader

        this.on('update', function () {
            this.items = opts.myitems
            this.header = opts.myheader
        })

    </script>

</people-list>